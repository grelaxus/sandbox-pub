********************************************************
somehow the invalid query 
curl -v localhost:9000 GET / HTTP/1.1 User-Agent: curl/7.35.0 Host: localhost:9000 Accept: */*
created 4 empty files in security-angular project ("Accept:", "GET", "Host:", "User-Agent") - this may be a security breach - To be checked
And it also spined following messages over security-angular-p3, security-angular-p4, security-angular-p5, which is weired
"...................
* Hostname was NOT found in DNS cache
 * Could not resolve host: Accept
 * Closing connection 6
 curl: (6) Could not resolve host: Accept
 * Hostname was NOT found in DNS cache
 * Could not resolve host: security-angular-p3
 * Closing connection 7
 curl: (6) Could not resolve host: security-angular-p3
 * Hostname was NOT found in DNS cache
 * Could not resolve host: security-angular-p3
 * Closing connection 8
............"

********************************************************
To test Authorization and Resource Servers run following commands: 
1. curl acme:acmesecret@localhost:9999/uaa/oauth/token -d 'grant_type=password&username=user&password=password&scope=read,write,trust'

2. TOKEN=46cde0ba-1191-45cd-9430-f6afd2a00763

3a. curl -H "Authorization: Bearer $TOKEN" localhost:9000

3b. curl -H "Authorization: Bearer $TOKEN" localhost:9999/uaa/user
{"details":{"remoteAddress":"127.0.0.1","sessionId":null,"tokenValue":"92e7b744-0acd-4a92-8ed2-d5ff74099c51","tokenType":"Bearer","decodedDetails":null},
"authorities":[{"authority":"ROLE_USER"}],
"authenticated":true,
"userAuthentication":
    {"details":
        {"grant_type":"password","username":"user","scope":"read,write,trust"},
     "authorities":[{"authority":"ROLE_USER"}],
     "authenticated":true,"principal":
        {"password":null,
         "username":"user",
         "authorities":[{"authority":"ROLE_USER"}],
         "accountNonExpired":true,
         "accountNonLocked":true,"credentialsNonExpired":true,
         "enabled":true},
     "credentials":null,"name":"user"},
     "oauth2Request":
        {"clientId":"acme","scope":["read,write,trust"],
         "requestParameters":{"grant_type":"password","username":"user","scope":"read,write,trust"},
         "resourceIds":[],"authorities":[{"authority":"ROLE_USER"}],"approved":true,"refresh":false,"redirectUri":null,"responseTypes":[],"extensions":{},
         "refreshTokenRequest":null,"grantType":"password"},
     "principal":{"password":null,"username":"user","authorities":[{"authority":"ROLE_USER"}],"accountNonExpired":true,"accountNonLocked":true,
     "credentialsNonExpired":true,"enabled":true},"credentials":"","clientOnly":false,"name":"user"}
Based on tutorial from David Sayer
http://spring.io/guides/tutorials/spring-security-and-angular-js/
http://presos.dsyer.com/decks/spring-security-angular.html

Part1. <strong>Project name:</strong> basic. <strong>Desc:</strong> Single backend, <strong>Security:</strong> HTTP Basic
Part2. single: Adds form authentication, Session cookie 
Part3. spring-session: Adds secure backend with custom token, Spring Session ID as token
Part4. proxy: UI acts as proxy. Session cookie, Spring Session
Part5. oauth2: Add OAuth2 SSO with a separate authentication server. Session cookie in UI and access token for backends
Part6. double: Add "admin" UI behind Gateway. Session cookie. Spring Session


Simple POST with curl:
curl --data '' http://localhost:8080/logout
more info on POST through curl: http://superuser.com/questions/149329/what-is-the-curl-command-line-syntax-to-do-a-post-request

**************************************************************************
DOCKER commands:
sudo service docker status/start/stop
sudo docker-compose up
telnet 127.0.0.1 6379

KEYS *
************************************************************************** 